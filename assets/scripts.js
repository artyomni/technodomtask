$(document).ready(function(){
	
	// Chanage dynamically product types in Add Product form
	$('#switcher-select').change(function(){
		var type_id = $(this).val();
		if(type_id){
			$.ajax({
				type:'GET',
				url: 'product_add.php?type='+type_id,
				success:function(data){
					
					if(data){
						$('.switcher').html(data);
					}
				},
				error: function(data){
					alert('Error: '+data);
				}
			});
		}
	});
	
	
	// Saves Add Product form
	$('.buttons').on('click','.save-form-button',function(){
		$.ajax({
			type:'POST',
			url: 'product_add.php',
			data: $('.form-add-new-product').serialize(),
			success:function(data){
				data =jQuery.parseJSON(data);
				if(data.success){
					
					// Show motification
					$('.notifications').html('<p class="ok">Successfully saved!</p>');
					
					// Clear all fields in the form
					$('.form-add-new-product input').each(function(){
						$(this).val('');
					});
					
					// Redirect to Product list after 1.5 sec
					setTimeout(function(){						
						window.location.replace('/products_list.php');
					},1500);
					
				}else{
					if(data.errortext){
						$('.notifications').html('<p class="err">'+data.errortext+'</p>');
					}else{
						$('.notifications').html('<p class="err">Error saving the form.</p>');
					}
					
				}
			},
			error: function(data){
				$('.notifications').html('<p class="err">Error saving the form.</p>');
			}
		});
		return false;
	});
	
	
	// Catch click on input checkbox in product list (to select and delete)	
	$('.product-list li').on('click','input[type="checkbox"]',function(){
	   if($(this).is(':checked')) {
		    // if selected, show Delete button
			$('#bulk-delete-button').show();
	   
	   }else{
		   
		   // check if at least 1 product is checked, then show Delete button, otherwise hide it
		   if($('.product-list li input:checked').length>0){
			   $('#bulk-delete-button').show();
		   }else{
			   $('#bulk-delete-button').hide();
		   }
	   }
	});
		
		
	// Click on button to delete selected products	
	$('#bulk-delete-button').on('click',function(){
		
		// confirm if deleting is not accedentally 
		if(!confirm('Are you sure you want to delete permanently these products?')){
			return false;
		}
		
		// delete
		$.ajax({
			type:'POST',
			url: 'products_list.php',
			data: $('.product-list-form').serialize(),
			success:function(data){
				data =jQuery.parseJSON(data);
				if(data.success){
					
					// notification success
					$('.notifications').html('<p class="ok">Selected products removed</p>');
					
					// remove selected products from the list
					$('.product-list li input:checked').each(function(){
						$(this).parent().fadeOut('slow',function(){
							$(this).remove();
						});
					});
					
					// clear notification after 2 sec
					setTimeout(function(){
						$('.notifications').html('');
					},2000);
					
				}else{
					if(data.errortext){
						$('.notifications').html('<p class="err">'+data.errortext+'</p>');
					}else{
						$('.notifications').html('<p class="err">Error removing the products.</p>');
					}
					
				}
			},
			error: function(data){
				$('.notifications').html('<p class="err">Error removing the products.</p>');
			}
		});
		return false;
	});

});