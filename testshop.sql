-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 08 2019 г., 11:23
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(15) NOT NULL,
  `sku` varchar(10) NOT NULL,
  `name` varchar(360) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `size` decimal(10,2) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `width` decimal(10,2) DEFAULT NULL,
  `length` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `size`, `weight`, `height`, `width`, `length`) VALUES
(17, '234rte', 'Acme DISC', '100.00', 1, '100.00', NULL, NULL, NULL, NULL),
(21, 'WP1356', 'War and Piece', '20.00', 2, NULL, '2.00', NULL, NULL, NULL),
(22, 'WCS8965', 'Where the Crawdads Sing', '14.39', 2, NULL, '0.30', NULL, NULL, NULL),
(23, 'SM9654', 'Educated: A Memoir', '43.45', 2, NULL, '0.65', NULL, NULL, NULL),
(24, 'UP6532', 'Unfreedom of the Press', '16.59', 2, NULL, '0.90', NULL, NULL, NULL),
(25, 'ABD5896', 'AmazonBasics 16x DVD+R Blank', '12.45', 1, '4700.00', NULL, NULL, NULL, NULL),
(26, 'SNY589', 'Sony DVD', '15.45', 1, '750.00', NULL, NULL, NULL, NULL),
(27, 'VRB9635', 'Verbatum', '25.00', 1, '700.00', NULL, NULL, NULL, NULL),
(28, 'MMRX5566', 'Memorax DVD+R', '21.55', 1, '4600.00', NULL, NULL, NULL, NULL),
(29, 'SAE99635', 'Solimo Alpha Engineered Wood 3-Door Wardrobe (Oak Finish)', '114.00', 3, NULL, NULL, '180.00', '120.00', '65.00'),
(30, 'MGL13235', 'MAGINELS Portable Closet', '43.00', 3, NULL, NULL, '150.00', '100.00', '55.00'),
(31, 'TBL965', 'REDCAMP Small Folding Table Adjustable', '33.99', 3, NULL, NULL, '75.00', '60.00', '120.00');

-- --------------------------------------------------------

--
-- Структура таблицы `product_types`
--

CREATE TABLE `product_types` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `fields` varchar(500) NOT NULL,
  `unit_title` varchar(2) NOT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_types`
--

INSERT INTO `product_types` (`id`, `title`, `fields`, `unit_title`, `note`) VALUES
(1, 'DVD', 'size', 'MB', 'Please provide size in <b>Mb</b>'),
(2, 'Books', 'weight', 'kg', 'Please provide weight of the item in <b>Kg</b>'),
(3, 'Furniture', 'height,width,length', 'cm', 'Please provide dimensions of the item in <b>cm</b>');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
