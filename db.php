<?php

# Credentials for DB connect
define('DB_HOSTNAME','localhost');
define('DB_DATABASE','testshop');
define('DB_USERNAME','root');
define('DB_PASSWORD','');

# Connecting to DB
try {
	$db = new PDO(
	'mysql:host='.DB_HOSTNAME.';dbname='.DB_DATABASE,
	DB_USERNAME, 
	DB_PASSWORD,
	array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
	);
} catch (PDOException $e) {
	die("Error trying to connect to the database. (".$e->getMessage().")");
}