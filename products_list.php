<?php 

# include DB connection
require_once 'db.php';

# add Product class
require_once 'ProductsClass.php';
$products = new ProductsClass($db);

# catch ajax for bulk product deleting
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	
	$bulk_remove = $products->deleteProductsBulk($_POST['id']);
	if ($bulk_remove==='1') {
		exit(json_encode(
			array(
				'success' => 1
			)
		));
	
	} else {
		exit(json_encode(
			array(
				'success' => 0,
				'errortext' => $bulk_remove
			)
		));
	}
}

# get list of products 
$list = $products->listProducts();

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>Product list</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<!-- CSS !-->
	<link rel="stylesheet" href="/assets/styles.css">
	
</head>
<body>

		<!-- Header !-->
		<div class="header">
			<div class="buttons">
				<a id="bulk-delete-button" class="button" href="#">Delete selected products</a>
				<a class="button" href="/product_add.php">+ Add a new product</a>
				
			</div>
			<h1>Product list </h1>	
		</div>
		
		<!-- Notifications here !-->
		<div class="notifications"></div>
		
		<!-- Product list !-->
		<form class="product-list-form">
			<ul class="product-list">
				<?php 
				if ($list) {
					foreach ($list as $item){
						$func_title = 'printTemplateType'.$item['type'];
						echo $products->$func_title($item);
					?>
					<?php
					}
				}?>
			</ul>
		</form>
		
		<!-- Scripts !-->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src='/assets/scripts.js?<?= time(); ?>'></script>
</body>
</html>