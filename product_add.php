<?php 

# include DB connection
require_once 'db.php';

# add Product class
require_once('ProductsClass.php');
$products = new ProductsClass($db);

# catch ajax requests
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	
	# dynamically show additional fields after type select  
	if ($_GET['type']) {
		die($products->getAddProductFields($_GET['type']));
	}
	
	# save Product Add form
	if ($_POST) {
		$save = $products->saveProductForm($_POST);
		if ($save==='1') {
			exit(json_encode(
				array(
					'success' => 1
				)
			));
		
		} else {
			exit(json_encode(
				array(
					'success' => 0,
					'errortext' => $save
				)
			));
		}
		
	} else {
		exit(json_encode(
			array(
				'success' => 0,
				'errortext' => 'Error: no POST params.'
			)
		));
	}
}

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>Product Add</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<!-- CSS !-->
	<link rel="stylesheet" href="/assets/styles.css">
	
</head>
<body>
	
	<!-- Header !-->
	<div class="header">
		<div class="buttons">
			<button class="button save-form-button" type="button">Save</button>
		</div>
		<h1><a class="disabled" href="/products_list.php">Product list</a> / Product Add</h1>
		
	</div>
	
	<!-- Notifications here !-->
	<div class="notifications"></div>
	
	<!-- Product Add form !-->
	<form class="form-add-new-product" method="POST">
		<label>SKU</label>
		<input type="text" name="sku">
		<label>Name</label>
		<input type="text" name="name">
		<label>Price (in US dollars)</label>
		<input type="text" name="price">
		
		<div class="switcher-block">
			<label>Type Switcher</label> 
			<select id="switcher-select" name="type">
				<?= $products->getSwitcherSelect($type); ?>
				
			</select>
		</div>
		
		<!-- Here dynamically show additional fields for Type Switcher !-->
		<div class="switcher"></div>
	</form>	
		
	<!-- Scripts !-->	
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>	
	<script src='/assets/scripts.js?<?=time();?>'></script>
	
</body>
</html>