<?php

/**
 * Product Class:
 * - display product list 
 * - add/remove new products
 *
 * by Artyom Ni <artyom.ni@gmail.com>
 */	
	 
class ProductsClass{
	
	private $db;
	
	function __construct($dbconn){
			
		 $this->db = $dbconn;	
		// # Credentials for DB connect
		// define('DB_HOSTNAME','localhost');
		// define('DB_DATABASE','testshop');
		// define('DB_USERNAME','root');
		// define('DB_PASSWORD','');
	
		// # Connecting to DB
		// try {
			// $this->db = new PDO(
			// 'mysql:host='.DB_HOSTNAME.';dbname='.DB_DATABASE,
			// DB_USERNAME, 
			// DB_PASSWORD,
			// array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
			// );
		// } catch (PDOException $e) {
			// die("Error trying to connect to the database. (".$e->getMessage().")");
		// }
		
	}
	
	
	# 3 Templates for product item display based on type ID
	public function printTemplateType1($item = []){
		$tmpl = '<li id="'.$item['id'].'">
			<input type="checkbox" name="id[]" value="'.$item['id'].'">
			<p class="sku">SKU: '.$item['sku'].'</p>
			<p class="name">'.$item['name'].'</p>
			<p class="price">$'.$this->clearDecimal($item['price']).'</p>
			<p class="'.$f.'">Size: '.$this->clearDecimal($item['size']).' MB</p>
		</li>';
		return $tmpl;
	}
	
	public function printTemplateType2($item = []){
		$tmpl = '<li id="'.$item['id'].'">
			<input type="checkbox" name="id[]" value="'.$item['id'].'">
			<p class="sku">SKU: '.$item['sku'].'</p>
			<p class="name">'.$item['name'].'</p>
			<p class="price">$'.$this->clearDecimal($item['price']).'</p>
			<p class="'.$f.'">Weight: '.$this->clearDecimal($item['weight']).' Kg</p>
		</li>';
		return $tmpl;
	}
	
	public function printTemplateType3($item = []){
		$tmpl = '<li id="'.$item['id'].'">
			<input type="checkbox" name="id[]" value="'.$item['id'].'">
			<p class="sku">SKU: '.$item['sku'].'</p>
			<p class="name">'.$item['name'].'</p>
			<p class="price">$'.$this->clearDecimal($item['price']).'</p>
			<p class="'.$f.'">
				Dimensions: 
				'.$this->clearDecimal($item['height']).'x'
				.$this->clearDecimal($item['width']).'x'
				.$this->clearDecimal($item['length']).' cm
			</p>
		</li>';
		return $tmpl;
	}
	
	
	# Format float values to delete zeros if no numbers after the dot
	public function clearDecimal($val){
		$val = (float) $val;
		return $val + 0;
	}
	
	# Build switcher select for product types in Product Add form
	public function getSwitcherSelect($current_type){
		$text = [];
		$q = $this->db->prepare("
			SELECT * 
			FROM `product_types`
		");	
		
		if (!$q->execute()) {
			die('DB errror #'.$q->errorInfo()[0].': '.$q->errorInfo()[2]);
		} else {
			
			$q->setFetchMode(PDO::FETCH_ASSOC);
			
			if ($q->rowCount() > 0) {
				$results = $q->fetchAll();
				$text[] = '<option value="">Select product type</option>';
				foreach ($results as $f) {
					$text[] = '<option '.($current_type==$f['id'] ? "selected" : "").' value="'.$f['id'].'">'.$f['title'].'</option>';
				}
				return join('',$text);
			} else {
				
				return false;
			}
			
		}
		
	}
	
	# Build additional fields based on product type in Product Add form 	
	public function getAddProductFields($type){
		
		$text = [];
		if(!$type){
			return false;
		}
		
		$q = $this->db->prepare("
			SELECT * 
			FROM `product_types`
			WHERE `id`=?
			LIMIT 0,1
		");	
		
		if (!$q->execute(array($type))) {
			die('DB errror #'.$q->errorInfo()[0].': '.$q->errorInfo()[2]);
		}else{
			
			$q->setFetchMode(PDO::FETCH_ASSOC);
			
			if ($q->rowCount() > 0) {
				$results = $q->fetchAll()[0];
				if ($results['fields']) {
					$results_exploded = explode(',',$results['fields']);
					foreach ($results_exploded as $f){
						$f = trim($f);
						$f_c = ucfirst($f);
						$text[] = '<label>'.$f_c.'</label> <input type="text" name="'.$f.'">';
					}
					
				}
				$text[] = '<p class="note">'.$results['note'].'</p>';
				return join('',$text);
			} else {
				return false;
			}
			
		}
		
	}
	
	# Save Product Add form
	public function saveProductForm($posts){
		$error_text = '';
			
		# checking if all $_POST are NOT empty
		foreach ($posts as $p) {
			if (empty(trim($p))) {
				$error_text .= '* All fields are required. <br>';
				break;
			}
		}
		
		# check if SKU already exists in DB 
		if ($posts['sku']) {
			if($this->skuExists($posts['sku'])){
				$error_text .= '* SKU exists in DB. Please indicate a different one<br>';
			}
		}
		
		# check if PRICE is numeric and decimal
		if ($posts['price']) {
			if (!$this->isDecimal($posts['price'])) {
				$error_text .= '* Please indicate the correct price (only numeric and decimal)<br>';
			}
		}
		
		# check if TYPE is selected
		if (!$posts['type']) {
			$error_text .= '* Please select a product type <br>';
		}
		
		# check if SIZE is numeric and decimal
		if ($posts['size']) {
			if (!$this->isDecimal($posts['size'])) {
				$error_text .= '* Please indicate the correct size (only numeric and decimal)<br>';
			}
		}
		
		# check if WEIGHT is numeric and decimal
		if ($posts['weight']) {
			if (!$this->isDecimal($posts['weight'])) {
				$error_text .= '* Please indicate the correct weight (only numeric and decimal)<br>';
			}
		}
		
		# check if HEIGHT is numeric and decimal
		if ($posts['height']) {
			if (!$this->isDecimal($posts['height'])) {
				$error_text .= '* Please indicate the correct height (only numeric and decimal)<br>';
			}
		}
		
		
		# check if WIDTH is numeric and decimal
		if ($posts['width']) {
			if (!$this->isDecimal($posts['width'])) {
				$error_text .= '* Please indicate the correct width (only numeric and decimal)<br>';
			}
		}
		
		# check if LENGTH is numeric and decimal
		if ($posts['length']) {
			if (!$this->isDecimal($posts['length'])) {
				$error_text .= '* Please indicate the correct length (only numeric and decimal)<br>';
			}
		}
		
		# save if no errors
		if (!$error_text) {
			$data = $posts;
			$save = $this->saveProductFormInDB($data);
			if ($save) {
				return '1';
			} else {
				return 'Oops! DB Error saving the form';
			}
		} else {
			return $error_text;
		}
		
	}
	
	# Check if value is numeric and decimal
	public function isDecimal($val){
		return is_numeric($val) && floor($vals) != $val;
	}
	
	# Check if input SKU unique
	public function skuExists($sku){
		
		$q = $this->db->prepare("
			SELECT `id`
			FROM `products`
			WHERE `sku`=?
			LIMIT 0,1
		");
				
		if (!$q->execute(array($sku))) {
			return false;
		}
		
		$q->setFetchMode(PDO::FETCH_ASSOC);
		if ($q->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	# Save Product Add form into DB
	private function saveProductFormInDB($posts){
		try {
			$data = [];	
			
			foreach ($posts as $k => $v) {
				if (empty($k) || $v==='') {
					continue;
				}
				$sets[] = " `".$k."` ";
				$qs[] = '?';
				$exec_array[] = $v;					
			}		
			
			$q = $this->db->prepare("
				INSERT INTO `products`
				(".join(',',$sets).")
				VALUES (".join(',',$qs).")
			");	
			
			if (!$q->execute($exec_array)) {
				die('DB errror #'.$q->errorInfo()[0].': '.$q->errorInfo()[2]);
			} else {
				return true;
			}	
			
		} catch(PDOException $e) {
			die("DB error: the form can not be saved (".$e->getMessage().")");
		}
		
	}
	
	# Get list of products
	public function listProducts(){
		
		$q = $this->db->prepare("
			SELECT *
			FROM `products`
			ORDER by `id` DESC
			LIMIT 0,100
		");
				
		if (!$q->execute()) {
			die('DB errror #'.$q->errorInfo()[0].': '.$q->errorInfo()[2]);
		}		
		
		$q->setFetchMode(PDO::FETCH_ASSOC);
		if ($q->rowCount() > 0) {
			$results = $q->fetchAll();
			return $results;
		} else {
			return false;
		}
				
		
	}
	
	# Delete selected products in bulk	
	public function deleteProductsBulk($ids = []){
		try {
			if (empty($ids)) {
				return 'Empty array of IDs';
			}
			
			$q = $this->db->prepare("
				DELETE FROM `products`
				WHERE `id` IN(".join(',',$ids).") 
			");	
			
			if (!$q->execute($exec_array)) {
				return 'DB error executing request';
			} else {
				return '1';
			}
		
		}catch(PDOException $e) {
			die("DB error: the items cannot be deleted (".$e->getMessage().")");
		}	
	}
	
}